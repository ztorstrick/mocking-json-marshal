package main

import (
	"errors"
	"testing"
)

func MockUnmarshal(data []byte, v interface{}) error {
	return errors.New("Mock error")
}

func TestGetStudentJsonErrorReturnsEmptyStudent(t *testing.T) {
	jsonStudent := "{\"FirstName\": \"Bart\", \"Age\": 12}"

	student := GetStudent(jsonStudent, MockUnmarshal)

	if (student.Age != 0) {
		t.Fatalf("Expected 0, got %d", student.Age)
	}
}