package main

import (
	"encoding/json"
	"log"
)

type Student struct {
	FirstName string
	Age       int
}

/* Declare a higher order function that can be injected as a dependency */
type (
	Unmarshal func(data []byte, v interface{}) (error)
)

func main() {
	jsonStudent := "{\"FirstName\": \"Bart\", \"Age\": 12}"
	student := GetStudent(jsonStudent, json.Unmarshal)
	log.Print(student)
}

/* Accept the unmarshal method instead of calling json.Unmarshal directly. */
func GetStudent(jsonStudent string, unmarshal Unmarshal) Student {
	var student Student
	err := unmarshal([]byte(jsonStudent), &student)

	if (err != nil) { return student }

	return student
}